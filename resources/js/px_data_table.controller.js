(function() {
	'use strict';

	/*
	* Controller to manage the datatable widget.
	* link: https://www.predix-ui.com/?show=px-data-table&type=component
	*/
	function PxdataTableCtrl($scope, $timeout, $element) {
		var ctrl = this,
		ctx = $scope.DECISYON.target.content.ctx,
		refObjId = ctx.instanceReference.value,
		target = $scope.DECISYON.target,
		mshPage = target.page,
		baseName = ctx.baseName.value,
		selectedItemsParamToExport = baseName + '_SelectedItems',
		DEFAULT_PAGESIZE = '10',
		DEFAULT_TRUE = 'true',
		DEFAULT_FALSE = 'false',
		unbindWatches = [],
		EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND',
		DEFAULT_TABLECOLUMNS = 'Display grid',
		wdgDataTable;

		/* Return the size input after adjusting its value by removing % and space white.
		The width and the height are adjusted because the Predix component doesn't supported values in percentage
		*/
		var getAdjustedSizing = function(size) {
			var adjustedSize = size.replace (/%/g, '').trim();
			return (parseInt(adjustedSize)) ? parseInt(adjustedSize) : '';
		};

		/* Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value.
		*/
		var setContainerSizing = function(context) {
			ctrl.height = getAdjustedSizing(context['min-height'].value);
			ctrl.width = getAdjustedSizing(context['min-width'].value);
		};

		/* Set the callback for data connector. Manage the static data connector and tghe dinamica dc(air-quality)*/
		var setCallbackDC = function(dataConnector) {
		    var promise;

		    if(angular.isFunction(dataConnector.instance.data)){
		        promise = dataConnector.instance.data();
		    }
		    else{
		        promise = dataConnector.instance.data;
		    }

			promise.then(function success(data) {
				setTableData(data);
			},function error(rejection) {
				setDataTableInError(rejection);
			});
		};

		/* Get data from the associated data connector*/
		var getTableData = function() {
			var dc, dataChart,
			useDataConnector = ctx.useDataConnector.value;
			if (useDataConnector) {
				if (target.getDataConnector) {
					dc = target.getDataConnector();
					dc.ready(function(dataConnector) {
						dataConnector.onUpdate(function(data) {
							setCallbackDC(dataConnector);
						});
						setCallbackDC(dataConnector);
					});
				} else {
					setDataTableInError({error : { errorMsg : 'The widget can\'t be inizialized because it needs to be associated to a data connector.'} });
					ctrl.tableData = [];
				}
			} else {
				setDataTableInError({error : {errorMsg : 'The Use data connector property is set to false.'}});
			}
		};

		/* Populate the data fetched through controller */
		var setTableData = function(data) {
			ctrl.tableData = data;
			ctrl.currentTpl = ctrl.tableView;
		};

		/* Function to handle DC failure */
		var setDataTableInError = function(rejection) {
		    resetAll();
			ctrl.errorMsg = rejection.error.errorMsg;
			ctrl.currentTpl = ctrl.errorView;
		};

		/* Check imported parameters: if defined, if empty */
		ctrl.getImportedParamFromContext = function(importedParam) {
			return (angular.isDefined(importedParam) && !angular.equals(importedParam.value, EMPTY_PARAM_VALUE)) ? importedParam.value : '';
		};

		/* Management of imported parameters
		- select the data in the table based on imported JSON */
		ctrl.manageImportedParams = function(selectedItemsFromParams) {
			var importedRows, rowToSet, rowToSetTrue = [],
			selectedRowsFromImport = [],
			i, j, k,
			wholeData = wdgDataTable.$.dataTable._internalData;
			if (jsonParse(selectedItemsFromParams).length > 0) {
				importedRows = jsonParse(selectedItemsFromParams);
				rowToSet = getSelectedRowsToSend(wholeData);
				for (i = 0; i < rowToSet.length; i++) {
					for (j = 0; j < importedRows.length; j++) {
						/* compare the imported row with complete data of the table */
						if (JSON.stringify(importedRows[j]) === JSON.stringify(rowToSet[i])) {
							selectedRowsFromImport.push(wholeData[i]);
						} else {
							/* reset the row if not satisfied */
							setAllRows(wholeData[i],false);
						}
					}
				}
				/* selecting the row passed from the import parameter */
				for (k = 0; k < selectedRowsFromImport.length; k++)
				{
					if ((selectedRowsFromImport[k]._selected === false)) {
						setAllRows(selectedRowsFromImport[k],true);
					}
				}
			} else {
				/* resetting the table if empty JSON in import parameter */
				for (i = 0; i < wholeData.length; i++) {
				setAllRows(wholeData[i],false); }
			}
		};

		/* set the rows to selected / unselected */
		var setAllRows = function(rowToSetTrue,isSelected) {
			var i,
			len;
			wdgDataTable.$.dataTable._setInternalDataAt(rowToSetTrue, '_selected', isSelected);
		};

		/* Exporting the param value */
		ctrl.sendParam = function() {
			if (mshPage) {
				target.sendParamChanged(selectedItemsParamToExport, ctrl.selectedItems);
			}
		};

        var resetAll = function(){
            ctrl.tableData = null;
            ctrl.pageSize = undefined;
			ctrl.striped = undefined;
			ctrl.filterable = undefined;
			ctrl.selectable = undefined;
			ctrl.showColumnChooser = undefined;
			ctrl.enableColumnResize = undefined;
			ctrl.tableColumns = undefined;
			ctrl.hidePaginationControl = undefined;
        };

		/* Management of data Table Properties */
		ctrl.manageParamsFromContext = function(context) {
			var hidePaginationControl = context.$pxhidePaginationControl.value,
			enableColumnResize = context.$pxenableColumnResize.value;
			var selectedItemsFromParams = ctrl.selectedItems !== '' ? ctrl.selectedItems : ctrl.getImportedParamFromContext(context['_PARAM_' + selectedItemsParamToExport]);
			ctrl.manageImportedParams(selectedItemsFromParams);
			ctrl.pageSize = (context.$pxpageSize.value !== '') ? context.$pxpageSize.value : DEFAULT_PAGESIZE;
			ctrl.striped = (context.$pxstriped.value !== '') ? context.$pxstriped.value : DEFAULT_TRUE;
			ctrl.filterable = (context.$pxfilterable.value !== '') ? context.$pxfilterable.value : DEFAULT_FALSE;
			ctrl.selectable = (context.$pxselectable.value !== '') ? context.$pxselectable.value : DEFAULT_FALSE;
			ctrl.showColumnChooser = (context.$pxshowColumnChooser.value !== '') ? context.$pxshowColumnChooser.value : DEFAULT_TRUE;
			ctrl.enableColumnResize = (enableColumnResize !== '') ? enableColumnResize : DEFAULT_TRUE;
			ctrl.tableColumns = (context.$pxtableColumns.value.value !== '') ? context.$pxtableColumns.value.value : DEFAULT_TABLECOLUMNS;
			ctrl.hidePaginationControl = (hidePaginationControl !== '') ? hidePaginationControl : DEFAULT_FALSE;
			setContainerSizing(context);
		};

		/* Set specific properties to DataTable attribute */
		ctrl.setAttributesToWidget = function() {
			ctrl.setBooleanAttributeOnlyIsTrue('striped',  ctrl.striped);
			ctrl.setBooleanAttributeOnlyIsTrue('page-size', ctrl.pageSize);
			ctrl.setBooleanAttributeOnlyIsTrue('filterable', ctrl.filterable);
			ctrl.setBooleanAttributeOnlyIsTrue('selectable', ctrl.selectable);
			ctrl.setBooleanAttributeOnlyIsTrue('show-column-chooser', ctrl.showColumnChooser);
			ctrl.setBooleanAttributeOnlyIsTrue('enable-column-resize', ctrl.enableColumnResize);
			ctrl.setBooleanAttributeOnlyIsTrue('table-columns', ctrl.enableColumnResize);
			ctrl.setBooleanAttributeOnlyIsTrue('hide-pagination-control', ctrl.hidePaginationControl);
			if (ctrl.tableColumns === 'Display as columns') {
				wdgDataTable.setAttribute('table-columns', 'true');
			} else {
				wdgDataTable.removeAttribute('table-columns', 'true');
			}
			if (ctrl.tableColumns === 'Display as rows') {
				wdgDataTable.setAttribute('table-rows', 'true');
			} else {
				wdgDataTable.removeAttribute('table-rows', 'true');
			}
		};

		/* Set the boolean attribute only if true because Predix component doesn't want the attribute in its DOM node if false*/
		ctrl.setBooleanAttributeOnlyIsTrue = function(attribute, boolValue) {
			var DataTableWdgPropertry = document.querySelector('#' + ctrl.widgetID);
			if (boolValue) {
				DataTableWdgPropertry.setAttribute(attribute, boolValue);
			}
		};

		/* When the predix ui widget was completed. Callback called when dcyOnLoad directive has completed. */
		ctrl.setPredixdataTableLibraryLoaded = function() {
			wdgDataTable = document.querySelector('#' + ctrl.widgetID);
			ctrl.manageParamsFromContext($scope.DECISYON.target.content.ctx);
			ctrl.addListenersToWidget();
			ctrl.setAttributesToWidget();
		};

		/* To convert stringfy array to an object
		- if invalid JSON in passed then return false.
		- valid JSON should be passed to the widget */
		var jsonParse = function(data) {
			try {
				return JSON.parse(data);
			} catch (e) {
				return false;
			}
		};

		/* Pushing the selected rows into an array */
		var selectedArray = function(e) {
			var selectedRows = [],
			i,
			data = e.target._internalData;
			for (i = 0;i < data.length;i++) {
				if (data[i]._selected === true) {
					selectedRows.push(data[i]);
				}
			}
			return selectedRows;
		};

		/*Return the selected rows to send as parameters and also, used to convert the format of the object with accordance to DataTable properties*/
         var getSelectedRowsToSend = function(selectedRows) {
            var i = 0,
                rowsToSend = [];
                console.log(selectedRows);
            for (i; i < selectedRows.length; i++){
                var row = selectedRows[i].row,
                    newRow = ctrl.tableData[row.dataIndex];
                rowsToSend.push(newRow);
            }
            return rowsToSend;
        };

		/* Add listener to table when a row is clicked.*/
		ctrl.addListenersToWidget = function() {
			var selectedAllRows,rowsToSend;
			if (wdgDataTable) {
				/* When table row is clicked */
				wdgDataTable.addEventListener('px-row-click', function(e) {
					$timeout(function() {
						selectedAllRows = selectedArray(e);
						rowsToSend = getSelectedRowsToSend(selectedAllRows);
						ctrl.selectedItems = angular.toJson(rowsToSend);
						ctrl.sendParam();
					});
				});
				/* When the select all button is clicked */
				wdgDataTable.addEventListener('px-select-all-click', function(e) {
					$timeout(function() {
						selectedAllRows = selectedArray(e);
						rowsToSend = getSelectedRowsToSend(selectedAllRows);
						ctrl.selectedItems = angular.toJson(rowsToSend);
						ctrl.sendParam();
					});
				});
			}
		};

		/* Includes functionality corresponding to selected template */
		$scope.$on('$includeContentRequested', function(angularEvent, src) {
			if (src == ctrl.tableView) {
				$timeout(function() {
					ctrl.listenerOnDataChange();
				});
			}
		});

		/* Routine called when angular element is destroied*/
		var destroyWidget = function() {
			var i;
			for (i = 0; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};

		/* Watch on widget context */
		ctrl.listenerOnDataChange = function() {
			unbindWatches.push($scope.$watch('DECISYON.target.content', function(newContent, oldContent) {
				if (!angular.equals(newContent, oldContent)) {
					ctrl.manageParamsFromContext(newContent.ctx);
				}
			}, true));
			/*Listener on destroy angular element; in this case we destroy all watch and listener*/
			$scope.$on('$destroy', destroyWidget);
		};

		/* Initialize */
		ctrl.inizialize = function() {
			ctrl.widgetID = 'pxdataTable_' + refObjId;
			ctrl.tableView = 'pxDataTable.html';
			ctrl.errorView = 'error.html';
			ctrl.selectedItems = '';
			getTableData(ctx);
		};
		ctrl.inizialize();
	}
	PxdataTableCtrl.$inject = ['$scope', '$timeout', '$element'];

	DECISYON.ng.register.controller('pxdataTablePredixCtrl', PxdataTableCtrl);
}());