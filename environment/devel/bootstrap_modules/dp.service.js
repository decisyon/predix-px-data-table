/**
 *
 * This module wrap the $http provider for dataprovider services and call mockdata service
 * Author : Pagano Francesco
 * Decisyon Srl
 */
(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name dcyApp.dcyDPService
   *
   * @requires dcyMockDataService
   * @requires dcyConstants
   * @requires $http
   * @requires $q
   * @requires $log
   *
   * @description
   * Factory for Manage mock data resources
   *
   *
   *
   * @example
   * ```js
   * function genericComponent(dcyDPService){
   *    var dpDefferer = dcyDPService({key:value});
   * }
   * myFn.$inject = ['dcyDPService'];
   *
   *
   * ```
   */
  function DPService($q, $http, $log, dcyEnvProvider, dcyMockDataService, dcyUtilsFactory, dcyCommonFnFactory, dcyConstants) {

    /**
     * Root Function return for factory
     * @param  {[type]} params    [Object]
     * @param status - server status response
     */
    function getData(params) {

      if (!dcyUtilsFactory.isValidObject(params)) {
        $log.error('#DPService #HTTP request; status: <DCY_ERROR> -> response: Parameters is not a valid object..');
        return false;
      }

      var
        deferred = $q.defer(),
        mockRequest = dcyMockDataService(params); //Mock data call if exist

      /**
       * @ngdoc method
       * @name responseObjectHttpRequest
       * @methodOf dcyApp.dcyDPService
       *
       * @description
       *  This method is used for check data response
       *
       * @param {object} response data from $http call.
       */
      function checkData(response) {
        if (angular.isDefined(params.dataFilter)) {
          var customResponse = response.data[params.dataFilter];
          if (angular.isDefined(customResponse)) {
            return deferred.resolve(customResponse);
          }

          $log.error('#DPService #HTTP request; status: <DCY_ERROR>-> response: your response filter not matching on data response');
          return deferred.reject(null);

        }
        return deferred.resolve(response);
      }


      /**
       * This method performs http call in each case
       */
      /**
       * @ngdoc method
       * @name responseObjectHttpRequest
       * @methodOf dcyApp.dcyDPService
       *
       * @description
       *  This method performs http call for get data response from server
       *
       */
      function runHttpRequest() {
        if ((!dcyUtilsFactory.isValidObject(params.ctx) && !dcyUtilsFactory.isValidString(params.rasterCtx)) || !dcyUtilsFactory.isValidString(params.dpc)) {
          $log.error('# DP Factory # Params Error for HTTP request; error status :  <DCY_ERROR>-> with this response : ctx, rasterCtx or dpc not valid');
          return deferred.reject(null);
        }

        //Set parameters for http request
        var paramsForRequest = {
	          revent: dcyUtilsFactory.isValidString(params.revent) ? params.revent : 'GET_DATA',
	          csMLO : csMLO,  // Questo settaggio deve essere spostato nella handled_event.service.js in quanto va previsto a pi� basso livello
	          a0: params.dpc,
	          a1: params.rasterCtx ? params.rasterCtx : angular.toJson(params.ctx)
//	          a1: params.rasterCtx ? params.rasterCtx : angular.toJson(dcyUtilsFactory.adapterForOldCtx(params.ctx)) , //Adapt new ctx format in old format
	        },
	        domainUrl = dcyEnvProvider.getItem('domainUrl', null, 'applicationInfo'),
	        contextPath = dcyEnvProvider.getItem('contextPath'),
        	serviceName = domainUrl + contextPath + '/cassiopeaWeb/report/empty.jsp.cas',
        	httpRequestType = angular.isDefined(params.httpRequestType) ? params.httpRequestType.toUpperCase() : 'POST', //Default request in post method
        	requestParams = {
				headers : {
					acceptCtx : 'V1'
				},
				method: httpRequestType,
				timeout: 7200000,
				url: serviceName,
				params: paramsForRequest,
				cache: angular.equals(httpRequestType, 'GET') ? true : false
	        };
        	
        	if(angular.equals(requestParams.method, 'POST')){
        		requestParams.headers['Content-Type'] = dcyConstants.HTTP_POST_CONTENT_TYPE;
        	}
        	
        $http(requestParams)
          .success(function(response, status) {
            if (dcyCommonFnFactory.responseValidator(response)) {
              checkData(response);
            } else {
              $log.error('#DPService #Error for HTTP request; status: <DCY_ERROR_ ' + status + '>-> response : response data is not valid');
              return deferred.reject(response);
            }
          })
          .error(function(response, status, headers, config) {
            $log.error('#DPService # Error for HTTP request; error status :  ', status, '-> response ', response);
            return deferred.reject({
              response: response,
              status: status,
              headers: headers,
              config: config
            });
          });
      }


      if (mockRequest === false) { //Mock Data is disabled
        runHttpRequest();
      } else {
        mockRequest.then(checkData, runHttpRequest);
      }

      return deferred.promise;
    }

    return getData;
  }
  
  
  
  
  
  DPService.$inject = ['$q', '$http', '$log', 'dcyEnvProvider', 'dcyMockDataService', 'dcyUtilsFactory', 'dcyCommonFnFactory', 'dcyConstants'];

  angular
    .module('dcyApp.services')
    .service('dcyDPService', DPService);

}());
